SELECT 
    o.order_id,
    o.customer_id,
    sum(p.amount) as amount
FROM {{ ref('stg_orders') }} o
LEFT JOIN {{ ref('stg_payments') }} p using(order_id)
GROUP BY 1, 2