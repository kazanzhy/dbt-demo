WITH 
customers as (
    SELECT * 
    FROM {{ ref('stg_customers')}}
    ),
orders as (
    SELECT * 
    FROM {{ ref('stg_orders') }}
    ),
payments as (
    SELECT * 
    FROM {{ ref('stg_payments') }}
    ),
customer_orders as (
    SELECT
        o.customer_id,
        min(order_date) as first_order_date,
        max(order_date) as most_recent_order_date,
        count(order_id) as number_of_orders,
        sum(p.amount) as lifetime_value
    FROM orders o
    LEFT JOIN payments p using(order_id)
    GROUP BY 1
    ),
final as (
    SELECT
        customers.customer_id,
        customers.first_name,
        customers.last_name,
        customer_orders.first_order_date,
        customer_orders.most_recent_order_date,
        coalesce(customer_orders.number_of_orders, 0) as number_of_orders,
        coalesce(customer_orders.lifetime_value, 0) as lifetime_value
    FROM customers
    LEFT JOIN customer_orders using (customer_id)
    )
SELECT * 
FROM final